﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests.Data
{
    public class MongoTestDbInitializer
        : IDbInitializer
    {
        private readonly IMongoCollection<Employee> _employeeCollection;
        private readonly IMongoCollection<Role> _roleCollection;

        public MongoTestDbInitializer(
            IMongoCollection<Employee> employeeCollection,
            IMongoCollection<Role> roleCollection)
        {
            _employeeCollection = employeeCollection;
            _roleCollection = roleCollection;
        }

        public void InitializeDb()
        {
            _roleCollection.DeleteMany(x => true);
            _employeeCollection.DeleteMany(x => true);

            _employeeCollection.InsertMany(FakeDataFactory.Employees);
            _roleCollection.InsertMany(FakeDataFactory.Roles);
        }
    }
}
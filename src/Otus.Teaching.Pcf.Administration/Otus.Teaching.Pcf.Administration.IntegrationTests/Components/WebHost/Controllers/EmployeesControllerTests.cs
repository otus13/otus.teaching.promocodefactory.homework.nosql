﻿using FluentAssertions;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Repositories;
using Otus.Teaching.Pcf.Administration.WebHost.Controllers;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests.Components.WebHost.Controllers
{
    public class EmployeesControllerTests : IClassFixture<MongoDatabaseFixture>
    {
        private readonly EmployeesController _employeesController;

        public EmployeesControllerTests(MongoDatabaseFixture mongoDatabaseFixture)
        {
            var employeesRepository = new MongoRepository<Employee>(
                mongoDatabaseFixture.EmployeeCollection, mongoDatabaseFixture.MongoSession);

            _employeesController = new EmployeesController(
                employeesRepository);
        }

        [Fact]
        public async Task GetEmployeeByIdAsync_ExistedEmployee_ExpectedId()
        {
            //Arrange
            var expectedEmployeeId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f");

            //Act
            var result = await _employeesController.GetEmployeeByIdAsync(expectedEmployeeId);

            //Assert
            result.Value.Id.Should().Be(expectedEmployeeId);
        }
    }
}
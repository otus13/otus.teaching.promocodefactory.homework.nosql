﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.IntegrationTests.Data;
using System;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests
{
    public class MongoDatabaseFixture : IDisposable
    {
        public MongoDatabaseFixture()
        {
            MongoClient = new MongoClient("mongodb://mongoadmin:docker@localhost");
            MongoDatabase = MongoClient.GetDatabase("test");

            EmployeeCollection = MongoDatabase.GetCollection<Employee>("employees");
            RoleCollection = MongoDatabase.GetCollection<Role>("roles");

            MongoSession = MongoClient.StartSession();

            new MongoTestDbInitializer(EmployeeCollection, RoleCollection)
                .InitializeDb();
        }

        public void Dispose()
        {
            MongoSession.Dispose();
        }

        public IMongoClient MongoClient { get; private set; }
        public IMongoDatabase MongoDatabase { get; private set; }
        public IMongoCollection<Employee> EmployeeCollection { get; private set; }
        public IMongoCollection<Role> RoleCollection { get; private set; }
        public IClientSessionHandle MongoSession { get; private set; }
    }
}
﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly IMongoCollection<Employee> _employeeCollection;
        private readonly IMongoCollection<Role> _roleCollection;

        public MongoDbInitializer(
            IMongoCollection<Employee> employeeCollection,
            IMongoCollection<Role> roleCollection)
        {
            _employeeCollection = employeeCollection;
            _roleCollection = roleCollection;
        }

        public void InitializeDb()
        {
            _roleCollection.DeleteMany(x => true);
            _employeeCollection.DeleteMany(x => true);

            _employeeCollection.InsertMany(FakeDataFactory.Employees);
            _roleCollection.InsertMany(FakeDataFactory.Roles);
        }
    }
}
﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using Otus.Teaching.Pcf.Administration.DataAccess.Repositories;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public static class ServiceCollectionDataAccessExtensions
    {
        public static IServiceCollection AddMongoDbClient(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            services
                .AddSingleton<IMongoClient>(_ =>
                    new MongoClient(configuration["MongoDb:ConnectionString"]))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoClient>()
                        .GetDatabase(configuration["MongoDb:Database"]))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoDatabase>()
                        .GetCollection<Employee>("employees"))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoDatabase>()
                        .GetCollection<Role>("roles"))
                .AddScoped(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoClient>()
                        .StartSession())
                .AddScoped(typeof(IRepository<>), typeof(MongoRepository<>))
                .AddScoped<IDbInitializer, MongoDbInitializer>();

            return services;
        }
    }
}
